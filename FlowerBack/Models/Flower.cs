﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FlowerBack.Models
{
    public class Flower
    {
        [Key]
        public int FlowerId { get; set; }

        [Required(ErrorMessage = "Usted necesita una {0}")]
        [StringLength(50, ErrorMessage = "El campo {0} puede contener maximo {1} y minimo {2} caracteres", MinimumLength = 1)]
        [Index("Flower_Description_Index", IsUnique = true)]
        public string Description { get; set; }

        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        public decimal Price { get; set; }

        [Display(Name = "Last Purchase")]
        //[DisplayFormat(DataFormatString = "{0:yyyy/mm/dd}", ApplyFormatInEditMode = false)]
        public DateTime? LastPurchase { get; set; }

        public string Image { get; set; }

        [Display(Name = "Is active")]
        public bool IsActive { get; set; }

        [DataType(DataType.MultilineText)]
        public string Observation { get; set; }

    }
}