﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FlowerBack.Models
{
    [NotMapped]
    public class FlowerRequest : Flower
    {
        public byte[] ImageArray { get; set; }
    }
}