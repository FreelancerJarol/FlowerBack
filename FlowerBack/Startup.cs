﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FlowerBack.Startup))]
namespace FlowerBack
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
